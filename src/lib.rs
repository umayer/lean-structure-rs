/// Representation of a 2D point.
#[repr(C)]
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Vector2d(pub f64, pub f64);

/// C version
///
/// Expects fields from a C++ `std::Vector<Eigen::Vector2d>`; returns the new, reduced
/// size of the `std::Vector`.
#[no_mangle]
pub extern "C" fn lean_structure_c(len: usize, data: *mut Vector2d) -> usize {
    let v = unsafe { std::slice::from_raw_parts_mut(data, len) };

    let mut a = 1;
    for b in 1..v.len() {
        let delta = ((v[b].0 - v[a - 1].0).abs(), (v[b].1 - v[a - 1].1).abs());
        if (delta.0 > 0.01) || (delta.1 > 0.01) {
            v[a] = v[b];
            a += 1;
        }
    }

    v[a - 1] = v.last().copied().unwrap();
    a
}

/// Remove points which are closer than 0.01 units in X or Y direction. Always keep at
/// least first and last points.
///
/// # Example
///
/// ```
/// # use lean_structure_rs::{Vector2d, lean_structure};
/// let data = vec![Vector2d(0.0, 0.0), Vector2d(0.0, 0.0), Vector2d(0.1, 0.0)];
/// let data = lean_structure(data);
///
/// assert_eq!(data, vec![Vector2d(0.0, 0.0), Vector2d(0.1, 0.0)]);
/// ```
pub fn lean_structure(mut data: Vec<Vector2d>) -> Vec<Vector2d> {
    let new_len = lean_structure_c(data.len(), data.as_mut_ptr());
    data.truncate(new_len);
    data
}

#[cfg(test)]
mod tests {
    #![allow(unused_imports)]
    use super::*;

    #[test]
    fn it_works() {
        let data = vec![
            Vector2d(0.0, 0.0),
            Vector2d(0.01, 0.0), // this is skipped
            Vector2d(0.02, 0.0), // this is overwritten with the last value
            Vector2d(0.03, 0.0),
        ];
        let ret = lean_structure(data);

        assert_eq!(ret, vec![Vector2d(0.0, 0.0), Vector2d(0.03, 0.0)]);
    }
}
