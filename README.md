# Rust-out-your-C

Dieses Repository dient als Beispiel, wie Rust mit C/C++ Code kombiniert werden kann.

Hier gehts weiter:
- [Beschreibung Funktionsweise](https://umayer.gitlab.io/lean-structure-rs/book)
- [Rust API Dokumentation](https://umayer.gitlab.io/lean-structure-rs/api/doc/lean_structure_rs)
