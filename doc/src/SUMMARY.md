# Summary

- [Wo bin ich hier?](where-am-i.md)
  <!-- + Was kommt hier -->

- [Aufbau und Infrastruktur](infrastructure.md)
  <!-- + Repository, -->
  <!-- + GitLab-Pages -->
  <!-- + mdBook, -->
  <!-- + GitLab-CI -->

- [Rust und C/C++]()
  <!-- + Rust macht lib -->
  <!-- + CMake gegen lib linken -->
  <!-- + Ein Wort zur C vs. C++ API -->
  <!-- + Ablauf -->
  <!--   * Build-System anpassen -->
  <!--   * gegen leere Lib linken -->
  <!--   * C-Funktion entfernen -> auf Fehler warten -->
  <!--   * C-Funktion nach Rust kopieren -> Fehler in Rust beseitigen -->
  <!--   * Tests schreiben -->
  <!--   * optimieren -->

- [CMake]()

- [Funktion im Service einbinden]()
  <!-- + Header-Datei -->
  <!-- + C++ Datei -->
  <!-- + Verweis auf Repository -->

- [Der Rust-Teil]()
  <!-- + Crate erstellen -->
  <!-- + Änderung in .toml -->
  <!-- + unsafe Rust-Funktion -->
  <!-- + safe Rust Wrapper -->
  <!-- + Tests -->
  <!-- + Doku -->
