# Wo bin ich hier?

Das hier ist eine statische Webseite, die mit dem selben Tool erstellt wurde, mit dem auch
das Rust Buch ([The Book](https://doc.rust-lang.org/book/)) geschrieben wird.

Dieses Tool heißt [mdBook](https://rust-lang.github.io/mdBook/). Es verwendet Markdown um
Online Bücher zu erstellen. Der Vorteil hieran ist, dass sich Links und Quellcode mit
Syntax-Highlighting besser erstellen lassen als mit Word-Dokumenten. Und... Markdown
können wir alle.

Diese Webseite hier ist Teil von GitLab. GitLab dient primär als Server für
Git-Repositories. Genau so, wie [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) als
Build-Server eingesetzt werden kann, bietet [GitLab
Pages](https://docs.gitlab.com/ee/user/project/pages/) die Möglichkeit die
API-Dokumentation "mit dem Repository" zusammen zu hosten.

Weil das cool und irgendwie interessant ist, beschreibt der folgende Abschnitt wie diese
Technologien zusammenhängen. Wo ist der Quellcode, wo liegt dieser Text, wo wird dieses
Online Buch hier generiert, etc.

In den darauf folgenden Kapiteln wird eine C++ Funktion aus dem Service genommen und
beschrieben, wie sich diese nach Rust portieren und einbinden lässt.
