# Aufbau und Infrastruktur

<div class="toc">
<!-- toc -->
</div>

Alle Daten zum Quellcode und zur Dokumentation, die hier vorgestellt werden, liegen ganz in diesem GitLab Repository: [https://gitlab.com/umayer/lean-structure-rs](https://gitlab.com/umayer/lean-structure-rs).

![Screenshot of the Project Page](img/gitlab-project-page.png#normal)


## Repository

Das [`doc`](https://gitlab.com/umayer/lean-structure-rs/-/tree/master/doc) Verzeichnis gehört nicht zur normalen Verzeichnisstruktur wie Rust und Cargo sie vorgeben ([Cargo Book](https://doc.rust-lang.org/cargo/guide/project-layout.html)), sondern enthält den Quellcode dieses Textes. Das Verzeichnis hätte auch anders heißen können.

Das Unterverzeichnis [`src`](https://gitlab.com/umayer/lean-structure-rs/-/tree/master/src), [`Cargo.toml`](https://gitlab.com/umayer/lean-structure-rs/-/tree/master/Cargo.toml) und [`.gitignore`](https://gitlab.com/umayer/lean-structure-rs/-/tree/master/.gitignore) wurden hingegen von Cargo angelegt. Sie gehören zur Rust Implementierung, die hier besprochen werden soll.

Wenn deine Datei [`README.md`](https://gitlab.com/umayer/lean-structure-rs/-/tree/master/README.md) existiert, wird diese in der Projektübersicht auf GitLab automatisch angezeigt - hier mit der Überschrift: "Rust-out-your-C".

Die Datei [`.gitlab-ci.yml`](https://gitlab.com/umayer/lean-structure-rs/-/tree/master/.gitlab-ci.yml) gehört zum "CI/CD" (Continuous Integration/Continuous Delivery) Teil von GitLab. Der Inhalt wird bei jedem Commit von GitLab bearbeitet, sobald eine Datei mit diesem Namen im Projektverzeichnis existiert.


## mdBook

`mdBook` ist ein Open Source Projekt auf [GitHub](https://github.com/rust-lang/mdBook) und steht unter der [Mozilla Public License 2.0](https://tldrlegal.com/license/mozilla-public-license-2.0-(mpl-2)). Die [API Dokumentation](https://rust-lang.github.io/mdBook/) ist kurz und bündig.

Die Installation erfolgt am Einfachsten über `cargo`:

```shell
cargo install mdbook
```

Die Verwendung ist denkbar einfach und lehnt sich sehr an das an, was bereits über `cargo` und Rust bekannt ist:

- `mdbook init Example` erstellt interaktiv ein neues Projektverzeichnis "Example" mit folgender Struktur:
  ```
  Example
  ├── book
  ├── book.toml
  └── src
      ├── chapter_1.md
      └── SUMMARY.md
  ```

  `book` ist das Zielverzeichnis, `book.toml` die globale Konfigurationsdatei. Der Inhalt liegt unterhalb des `src` Verzeichnisses. `SUMMARY.md` ist der Einstiegspunkt in die einzelnen Kapitel und verlinkt in alle anderen Markdown Dateien - z. B. `chapter_1.md`.

- `mdbook build` erstellt das Buch,
- `mdbook serve` liefert den Inhalt lokal einem Browser unter `localhost:3000` aus,
- usw.

Der Inhalt ist normales [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet). Über "Preprozessor" Plugins lassen sich aber auch Gantt-Diagramme, Grafen oder Flowcharts nachrüsten:


```
    ```mermaid
    graph TD;
        A-->B;
        A-->C;
        B-->D;
        C-->D;
    ```
```

```mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
```


## GitLab-Pages

GitLab kann für jedes Repository zusätzlich eine statische Webseite unter einer festen URL anzeigen. So eine Webseite zu einem Repository nennt sich "GitLab-Pages".

Um für ein Repository solch eine statische Seite anzulegen, muss das CI/CD Build-System von GitLab verwendet werden. Voraussetzung dafür ist, dass es in `.gitlab-ci.yml` eine Regel mit dem Namen `pages` gibt die dazu führt, dass Dateien in einem `public` Verzeichnis ablegt werden. Alles was am Schluss in diesem Verzeichnis  liegt, steht hinterher als statische Webseite unter einem fest definierten Link zur Verfügung. Hier lautet diese URL: [https://umayer.gitlab.io/lean-structure-rs/book](https://umayer.gitlab.io/lean-structure-rs/book).

Der genaue Ort der "Pages" lässt sich unter *Settings* \\(\rightarrow\\) *Pages* nachschlagen:

![Settings -> Pages](img/settings-pages.png#normal2)


## GitLab-CI/CD

GitLab-CI/CD übernimmt die Aufgabe eines Build-Servers, wie z. B. Jenkins. Bei der Datei `.gitlab-ci.yml` handelt es sich um eine Textdatei. Das Format ist mächtiger als Windows ".ini"-Dateien, aber besser lesbar als XML. Hier die Bedeutung der einzelnen Abschnitte:

```yml
stages:
  - build
  - deploy
```

`stages` definiert Abschnitte, die im Build-Prozess getrennt voneinander durchgeführt werden können. Es ist möglich einzelne Schritte fürs compilieren, testen, Dokumentation erstellen, Ergebnis hoch laden, etc. zu erstellen. Einzelne Stages können voneinander abhängen und werden u. U. auf unterschiedlichen Rechnern (auch Slaves oder Runner genannt) ausgeführt. Es ist egal, Wie die Schritte heißen (hier: `build` und `deploy`). Man kann auch alles in einem Schritt durchführen - was zumindest den Vorteil hat, dass alle Schritte auf demselben Rechner ausgeführt werden.


```yml
build-all:
  stage: build
  needs: []
  tags:
    - rust
  script:
    - cargo fmt -- --check -v   # code formatting: abort on mismatch
    # The following actually aborts the build if the static code analysis tool (linter)
    # find any issues. For the purpose of this example it is only commented-tut.
    # - cargo clippy
    - cargo build -vv
    - cargo test
```

Dieser Block definiert eine Regel namens `build-all`. Der Name ist frei wählbar. Als nächstes wird konfiguriert, dass diese Regel zum `build` Schritt gehört und von keinen anderen Regeln abhängig ist (`needs: []`).

`tags` ist eine Liste von Dingen, die ein Rechner "haben muss", wenn er diese Regel ausführen soll. Dabei kann es sich um installierte Software handeln ("vs2019", "vs2017", "mdbook", "rust-1.47"), um Betriebssystemangaben ("windows", "linux") oder Architekturen ("arm", "amd64"), etc.. Wichtig ist nur, dass die ausführenden Rechner und diese Tags sich darüber einig sind, was unter einer Angabe wie "rust" zu verstehen ist. Ansonsten sind die Bezeichner frei wählbar.

Der letzte Abschnitt führt eine Liste von Shell-Befehlen auf, die nacheinander ausgeführt werden um diese Regel durchzuführen. Die Regel wird mit einem Fehler abgebrochen, wenn eines der Befehle unter `script` nicht erfolgreich beendet wird.

Der nächste Abschnitt ist für das Erzeugen dieses Online-Buchs und der Rust-API zu diesem Code-Beispiel verantwortlich:

```yml
pages:
  stage: deploy
  needs: ["build-all"]
  tags:
    - mdbook, mdbook-toc
  script:
    - mdbook build $CI_PROJECT_DIR/doc --dest-dir $CI_PROJECT_DIR/public/book
    - cargo doc --target-dir public/api
```

Diese Regel hängt von `build-all` ab. Der betreffende Rechner muss die Programme `mdbook` und `mdbook-toc` installiert haben.

Der letzte Abschnitt sorgt dafür, dass das `public` Verzeichnis als Ergebnis dieser Regel gekennzeichnet wird. Es ist ein sogenanntes "Build-Artefakt". Das führt dazu, dass GitLab dieses Verzeichnis archiviert - bis die Artefakte durch Neuere überschrieben werden:

```yml
artifacts:
  paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
```

Die Bedingung in dieser Regel sorgt dafür, dass nur Änderungen im Zweig `master` übernommen werden.

## Slave / Runner

Wer führt diese Build-Regeln alle aus, wo wird die Software und die Dokumentation compiliert und gebaut?

Besitzer und Entwickler eines GitLab Repositories müssen selbst einen Rechner so konfigurieren, dass ihre Software (samt Dokumentation) damit gebaut werden kann. Auf einem solchen Rechner wird ein Go-Programm von GitLab installiert, der sog. [`GitLab-Runner`](https://docs.gitlab.com/runner/).

In den Projekteinstellungen *Settings* \\(\rightarrow\\) *CI/CD* lassen sich neue Runner für ein Projekt hinzufügen. Es ist auch möglich öffentlich verfügbare Runner zuzulassen.

![GitLab Runner Konfigurationsseite](img/ci-cd-runner.png#normal)

Um einen eigenen Runner hinzuzufügen, wird im Wesentlichen ein einzelnes .exe-Go-Programm in ein lokales Verzeichnis kopiert und dort `gitlab-runner register` aufgerufen:

[![Register a GitLab Runner](img/register-runner.png)](img/register-runner.png#normal)

## Ablauf

Das Zusammenspiel funktioniert wie folgt:

Wenn ein neuer Commit nach GitLab gepusht wird, schaut GitLab nach ob eine `.gitlab-ci.yml` Datei vorhanden ist und führt diese aus.

GitLab stellt fest, dass zuerst die Regel `build-all` ausgeführt werden muss, da diese Regel keine Abhängigkeiten besitzt. Ein Runner, der diese Regel ausführen kann, muss den Tag `rust` besitzen.

In den Projekteinstellungen für dieses Repository sind die öffentlichen Runner deaktiviert. Aber für dieses Projekt wurde ein eigener Runner registriert, der über die Tags `mdbook`, `mdbook-toc`, `rust` verfügt. Der Runner ist online und bereit Aufträge entgegen zu nehmen, also stößt GitLab das Ausführen dieser Regel an. Die Regel wird erfolgreich abgearbeitet, die Bibliothek wird gebaut, die Tests laufen durch.

Der nächste Schritt ist das Ausführen der `deploy` Regel, die jetzt möglich ist, weil die Abhängigkeiten (`build-all`) alle erfüllt sind. Als einziger Runner kommt wieder der private Runner in Frage, der dieses Online-Buch (`mdbook`) und die API Dokumentation zur Bibliothek (`cargo doc`) baut. Die Ergebnisse landen wegen der Befehlsparameter in `.../public/book` und `.../public/api`.

Nach dem erfolgreichen Abschluss dieser speziellen `deploy`-Regel sucht GitLab nach einem `public`-Verzeichnis, kopiert den Inhalt in einen anderen Bereich und macht diesen über eine andere URL zugänglich.
