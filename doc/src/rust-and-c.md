# Rust und C/C++

<div class="toc">
<!-- toc -->
</div>


Dieser Abschnitt behandelt grob die notwendigen Grundlagen für die Interaktion von Rust mit C/C++ Code. Die (Rust) Grundlagen hierzu finden sich in:

- [Das Buch, Kapitel 19.1 - Unsafe Rust](https://doc.rust-lang.org/book/ch19-01-unsafe-rust.html)
- [Rustonomicon, Kapitel 5.9 - Foreign Function Interface (FFI)](https://doc.rust-lang.org/book/ch19-01-unsafe-rust.html)
- [Cargo Book, Kapitel 3.8 - Rust Build Skripte](https://doc.rust-lang.org/cargo/reference/build-scripts.html)

## Ansatz

Das prinzipielle Vorgehen ist wie folgt:

Es wurde eine (verhältnismäßig einfache) Funktion aus dem Service ausgewählt, die nach Rust portiert werden soll: `leanStructure()`.

[zPathPlanning.h:78](https://gitlab.com/z-laser/zlp/-/blob/master/zlp-service/src/zService/zProjector/zPathPlanning.h#L78)
```cpp
//! Entferne sinnlose Punkte (doppelte) und Linien (übereinanderliegende)
static void leanStructure(Polyline2d& polyline);
```

[zPathPlanning.cpp:47](https://gitlab.com/z-laser/zlp/-/blob/master/zlp-service/src/zService/zProjector/zPathPlanning.cpp#L47)
```cpp
void zPathPlanning::leanStructure(Polyline2d& polyline)
{
    Polyline2d orig = polyline;  // Kopie
    unsigned int cntValid = 1;   // Erster bleibt drin

    // Entferne doppelte Punkte
    // Anfang und Ende sollten danach immernoch gleich sein!!
    for (unsigned int actIdx = 1; actIdx < orig.size(); actIdx++) {
        Eigen::Vector2d dist = orig[actIdx] - polyline[cntValid - 1];
        if (fabs(dist[0]) > 0.01 || fabs(dist[1]) > 0.01) {
            polyline[cntValid] = orig[actIdx];
            cntValid++;
        }
    }
    polyline[cntValid - 1] = orig.back();
    polyline.resize(cntValid);
}
```

Diese funktion ist "einfach", weil `Polyline2d&` im Wesentlichen nur ein STL-Vector mit `Eigen::Vector2d` Parameter ist:

[zlGeometryTypes.h:56](https://gitlab.com/z-laser/zlp/-/blob/master/zlp-service/src/zService/zlGeometryTypes.h#L56)
```cpp
//! Definition eine Polyline-Listentyps 2D
//! Fürs Alignment spezieller Vector nötig:
//! siehe \see http://eigen.tuxfamily.org/dox-devel/group__TopicUnalignedArrayAssert.html
class Polyline2d :
        public std::vector<Eigen::Vector2d, Eigen::aligned_allocator<Eigen::Vector2d>>,
        public ProjectablePolyline
{
  public:

    Polyline2d(size_t size);
    Polyline2d() {}
};
```

Es wäre wesentlich aufwändiger

https://doc.rust-lang.org/cargo/reference/cargo-targets.html?highlight=crate-type#the-crate-type-field

erstellt normalerweise "Rust Bibliotheken".

## Ein Wort zu C++ und Rust

- https://github.com/rust-qt/ritual
- https://stackoverflow.com/questions/52923460/how-to-call-a-c-dynamic-library-from-rust/52940455
- https://github.com/google/rustcxx
- https://users.rust-lang.org/t/how-to-return-a-vector-from-c-back-to-rust-ffi/36569/4
